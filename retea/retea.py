# Import libraries
import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import keras
from tensorflow.python.tools import freeze_graph
from tensorflow.python.tools import optimize_for_inference_lib
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from keras import backend as K
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D, Conv2D
from keras.optimizers import SGD, RMSprop, adam
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model

K.common.image_dim_ordering()

########## VARIABILE
PATH = os.getcwd()  # pathul alctual unde se desfasoara actiunile
data_path = PATH + '/data'  # pathul pozelor care urmeaza sa fie clasificate
data_dir_list = os.listdir(data_path) # lista cu foldere disponibile in folderul de poze

imgHeight = 128  # inaltime imagine
imgWidth = 128  # latime imagine
colorsNumber = 1  # numarul de culori folosite (3-rgb)
num_epoch = 20  # numarul de epoci pentru invatare

num_classes = 3 # Numarul de categorii pentru andrenare --> 3 (Audi,VW,Opel)
img_data_list = []

############## CITIRE Imagine
for dataset in data_dir_list:
    img_list = os.listdir(data_path+'/' + dataset)
    print('Loaded the images of dataset-'+'{}\n'.format(dataset))

    for img in img_list:
        input_img = cv2.imread(data_path + '/' + dataset + '/' + img) ## citire imagine 
        input_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2GRAY) ## alb-negru
        input_img_resize = cv2.resize(input_img, (128, 128)) #resize 
        img_data_list.append(input_img_resize) # adaugare intr-o lista 

#### PRELUCRAREA DATELOR 
img_data = np.array(img_data_list) #trasnformare in matrice
img_data = img_data.astype('float32') #transformam totul in valori float 
img_data /= 255 
print(img_data.shape)

if colorsNumber == 1:
    if K.common.image_dim_ordering():
        img_data = np.expand_dims(img_data, axis=1)
        print(img_data.shape)
    else:
        img_data = np.expand_dims(img_data, axis=4)
        print(img_data.shape)

else:
    if K.image_dim_ordering() == 'th':
        img_data = np.rollaxis(img_data, 3, 1)
        print(img_data.shape)


############### GENERARE RASPUNSRI -> ONE HOT ENCODING 
num_of_samples = img_data.shape[0]  #numarul de date de antrenament 
labels = np.ones((num_of_samples,), dtype='int64') #Generarre o lista de int cu n elemente toate  avand valoarea intiala 1

labels[0:527] = 0 #Audi
labels[527:829] = 1 #Opel
labels[829:] = 2 #Vw

Y = np_utils.to_categorical(labels, num_classes) #one hot encoding 


###### SEPARAREA DATELOR 
x, y = shuffle(img_data, Y, random_state=2) #amestecare date 
X_train, X_test, y_train, y_test = train_test_split( x, y, test_size=0.13, random_state=2) #separare date in date de antrenament si de testare 

############################ CREARE RETEA NERONALA

input_shape = img_data[0].shape
print("Input:")
print(input_shape)
model = Sequential()
model.add(Convolution2D(32, (3, 3), border_mode='same',input_shape=input_shape, data_format='channels_first'))
model.add(Activation('relu'))
model.add(Convolution2D(32, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Dropout(0.5))
model.add(Convolution2D(64, 3, 3))
model.add(Activation('relu'))
#model.add(Convolution2D(64, 3, 3))
# model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Dropout(0.5))
model.add(Flatten())
model.add(Dense(128))
model.add(Activation('relu'))
# model.add(Dropout(0.5))
model.add(Dense(num_classes))
model.add(Activation('softmax'))


######## SETARE FUNCTILOR DE OPTIMIZARE , CALCULARE A COSTULUi
#sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
#model.compile(loss='categorical_crossentropy', optimizer=sgd,metrics=["accuracy"])
model.compile(loss='categorical_crossentropy',
optimizer='adam', metrics=["accuracy"])

model.summary()

tbCallBack = keras.callbacks.TensorBoard(
    log_dir='./Graph', histogram_freq=0, write_graph=True, write_images=True)




datagen = ImageDataGenerator(
    zoom_range=0.2,  # randomly zoom into images
    # randomly rotate images in the range (degrees, 0 to 180)
    rotation_range=10,
    # randomly shift images horizontally (fraction of total width)
    width_shift_range=0.1,
    # randomly shift images vertically (fraction of total height)
    height_shift_range=0.1,
    horizontal_flip=True,  # randomly flip images
    vertical_flip=False)  # randomly flip images

# Training
# hist = model.fit(X_train, y_train, batch_size=16, nb_epoch=num_epoch, verbose=1, validation_data=(X_test, y_test),callbacks=[tbCallBack])
batch_size = 16

model = load_model('my_model.h5')

hist = model.fit_generator(datagen.flow(X_train, y_train, batch_size=batch_size),
                           steps_per_epoch=int(
                               np.ceil(X_train.shape[0] / float(batch_size))),
                           epochs=30,
                           validation_data=(X_test, y_test))


export_model(tf.train.Saver(), model, [
             "conv2d_1_input"], "activation_5/Softmax")
model.save('my_model.h5')


############# EXPORT NEURONAL NETWORK
MODEL_NAME = 'retea'

def export_model(saver, model, input_node_names, output_node_name):
    tf.train.write_graph(K.get_session().graph_def, 'out',
                         MODEL_NAME + '_graph.pbtxt')

    saver.save(K.get_session(), 'out/' + MODEL_NAME + '.chkp')

    freeze_graph.freeze_graph('out/' + MODEL_NAME + '_graph.pbtxt', None,
                              False, 'out/' + MODEL_NAME + '.chkp', output_node_name,
                              "save/restore_all", "save/Const:0",
                              'out/frozen_' + MODEL_NAME + '.pb', True, "")

    input_graph_def = tf.GraphDef()
    with tf.gfile.Open('out/frozen_' + MODEL_NAME + '.pb', "rb") as f:
        input_graph_def.ParseFromString(f.read())

    output_graph_def = optimize_for_inference_lib.optimize_for_inference(
        input_graph_def, input_node_names, [output_node_name],
        tf.float32.as_datatype_enum)

    with tf.gfile.FastGFile('out/opt_' + MODEL_NAME + '.pb', "wb") as f:
        f.write(output_graph_def.SerializeToString())

    print("graph saved!")
