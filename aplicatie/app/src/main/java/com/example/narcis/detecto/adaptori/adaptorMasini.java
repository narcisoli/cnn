package com.example.narcis.detecto.adaptori;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.narcis.detecto.R;
import com.example.narcis.detecto.StaticValues;
import com.example.narcis.detecto.models.Masina;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class adaptorMasini extends ArrayAdapter<Masina> {


    private int layoutResource;
    private Masina masina;
    private ImageView imageView;
    private TextView textNume;
    private TextView textAcuratete;


    public adaptorMasini(Context context, int layoutResource, List<Masina> pizzalist) {
        super(context, layoutResource, pizzalist);
        this.layoutResource = layoutResource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);

        }

        imageView=(ImageView) view.findViewById(R.id.adaptorImagine);
        textNume=(TextView)view.findViewById(R.id.adaptorNume);
        textAcuratete=(TextView)view.findViewById(R.id.adaptorAcuratete);

        masina = getItem(position);

        if (masina != null) {
            textNume.setText(masina.getNume());
            textAcuratete.setText(String.format("%.2f",masina.getAcuratete()*100)+" %");
        }

        return view;
    }


}