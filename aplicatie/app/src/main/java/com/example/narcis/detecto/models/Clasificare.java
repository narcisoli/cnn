package com.example.narcis.detecto.models;

public class Clasificare {
    private float probabilitate;
    private String nume;

    public Clasificare(float probabilitate, String nume) {
        this.probabilitate = probabilitate;
        this.nume = nume;
    }
    public Clasificare(){
        this.probabilitate=-1f;
        this.nume="";
    }

    public void Update(float probabilitate,String nume){
        this.probabilitate = probabilitate;
        this.nume = nume;
    }


    public float getProbabilitate() {
        return probabilitate;
    }

    public void setProbabilitate(float probabilitate) {
        this.probabilitate = probabilitate;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }
}
