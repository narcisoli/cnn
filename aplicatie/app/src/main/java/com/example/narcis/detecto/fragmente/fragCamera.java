package com.example.narcis.detecto.fragmente;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.narcis.detecto.R;
import com.github.florent37.camerafragment.CameraFragment;
import com.github.florent37.camerafragment.configuration.Configuration;

public class fragCamera extends Fragment {

    private  static  fragCamera instance;
    private CameraFragment cameraFragment;

    public static fragCamera getInstance(){
        if (instance==null)
            instance=new fragCamera();
        return instance;
    }

    private View myView;

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {



            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.cameraFRAG, cameraFragment, "camera")
                    .commit();


        super.onViewStateRestored(savedInstanceState);


    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.fragment_camera, container, false);
          cameraFragment = CameraFragment.newInstance(new Configuration.Builder().build());

            getActivity().getSupportFragmentManager().beginTransaction()
                   .replace(R.id.cameraFRAG, cameraFragment, "camera")
                  .commit();


        return myView;
    }


}
