package com.example.narcis.detecto.models;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TensorFlow {

    private TensorFlowInferenceInterface tfHelper;
    private String inputName;
    private String outputName;
    private int inputSize;
    private List<String> labels;
    private float[] output;
    private String[] outputNames;


    public static TensorFlow create(AssetManager assetManager,
                                    String path, List<String> labels, int inputSize, String inputName, String outputName) throws IOException {
        TensorFlow tf = new TensorFlow();



        tf.inputName = inputName;
        tf.outputName = outputName;
        tf.labels = labels;
        tf.tfHelper = new TensorFlowInferenceInterface(assetManager, path);
        int numClasses = labels.size();
        tf.inputSize = inputSize;


        tf.outputNames = new String[]{outputName};

        tf.outputName = outputName;
        tf.output = new float[numClasses];


        return tf;
    }

    public Clasificare recognize(final Bitmap bitmap) {
        float[] floatValues = new float[bitmap.getHeight() * bitmap.getWidth() ];

        for (int i = 0; i < bitmap.getHeight(); i++) {
            for (int j = 0; j < bitmap.getWidth(); j++) {
               int pixel = bitmap.getPixel(i, j);
                floatValues[i  + j * bitmap.getHeight()]= (float) ((Color.red(pixel)+Color.green(pixel)+Color.blue(pixel))/765.0);
            }
        }

        tfHelper.feed(inputName, floatValues, 1, 1, inputSize, inputSize);
        tfHelper.run(outputNames);
        tfHelper.fetch(outputName, output);

        Clasificare ans = new Clasificare();
        for (int i = 0; i < output.length; ++i) {
            System.out.println(labels.get(i) + " : " + output[i]);

            if (output[i] > Global.valoareMinima && output[i] > ans.getProbabilitate()) {
                ans.Update(output[i], labels.get(i));
            }
        }

        return ans;
    }
}
