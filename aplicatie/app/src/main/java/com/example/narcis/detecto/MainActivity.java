package com.example.narcis.detecto;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.narcis.detecto.fragmente.fragCamera;
import com.example.narcis.detecto.fragmente.fragInfo;
import com.example.narcis.detecto.fragmente.fragUpload;
import com.example.narcis.detecto.models.Global;
import com.example.narcis.detecto.models.Masina;
import com.example.narcis.detecto.models.TensorFlow;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static TensorFlow retea;
    private ViewPager pager;
    private BottomBar bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Masina> list=new ArrayList<>();
        list.add(new Masina(1,"Audi","", (float) 0.63,60));
        list.add(new Masina(2,"Bmw","", (float) 0.63,60));
        list.add(new Masina(3,"Opel ","", (float) 0.78,60));
        list.add(new Masina(4,"Volkswagen","", (float) 0.54,60));
        StaticValues.lista=list;

        pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        pager.setOffscreenPageLimit(4);


        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {

            @Override
            public void onTabSelected(int tabId) {
               if(tabId==R.id.tab_detect)
                   pager.setCurrentItem(0);
               else
                   pager.setCurrentItem(1);
            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomBar.selectTabAtPosition(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        try {
            loadTensorflow();
        } catch (IOException e) {
            Toast.makeText(MainActivity.this, "Nu s-a incarcat reteaua neuronala", Toast.LENGTH_SHORT).show();
        }
    }


    private void loadTensorflow() throws IOException {
        List<String> labels;
        labels=StaticValues.getLabels();

        retea = TensorFlow.create(getAssets(), "opt_retea82crop.pb", labels, Global.dimensiune,
                "conv2d_1_input", "dense_2/Softmax");
    }


    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return fragUpload.getInstance();
                case 1:
                    return fragInfo.getInstance();

                default:
                    return fragCamera.getInstance();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }






}
