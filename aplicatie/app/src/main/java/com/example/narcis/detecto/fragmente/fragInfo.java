package com.example.narcis.detecto.fragmente;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.narcis.detecto.R;
import com.example.narcis.detecto.StaticValues;
import com.example.narcis.detecto.adaptori.adaptorMasini;

public class fragInfo extends Fragment {

    private  static  fragInfo instance;
    private ListView listView;
    public adaptorMasini adaptor;
    private TextView textStatistici;

    public static fragInfo getInstance(){
        if (instance==null)
            instance=new fragInfo();
        return instance;
    }

    private View myView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.fragment_info, container, false);

        listView=(ListView)myView.findViewById(R.id.listView);
        adaptor=new adaptorMasini(myView.getContext(),R.layout.adaotor_masini, StaticValues.lista);

        textStatistici=(TextView)myView.findViewById(R.id.textStatistici);

        resetAccuracy();

        listView.setAdapter(adaptor);
        return myView;
    }

    public void resetAccuracy() {
        float aux=0;
        for(int i = 0 ; i < StaticValues.lista.size();i++)
            aux+= StaticValues.lista.get(i).getAcuratete();
        aux/=StaticValues.lista.size();
        textStatistici.setText("Total accuracy : "+String.format("%.2f",aux*100) + "%");
    }


}
