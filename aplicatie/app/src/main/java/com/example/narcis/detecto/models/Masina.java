package com.example.narcis.detecto.models;

public class Masina {
    int Id;
    String nume;
    String Detalii;
    Float Acuratete;
    int NrPredictii;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getDetalii() {
        return Detalii;
    }

    public void setDetalii(String detalii) {
        Detalii = detalii;
    }

    public Float getAcuratete() {
        return Acuratete;
    }

    public void setAcuratete(Float acuratete) {
        Acuratete = acuratete;
    }

    public int getNrPredictii() {
        return NrPredictii;
    }

    public void setNrPredictii(int nrPredictii) {
        NrPredictii = nrPredictii;
    }

    public Masina(int id, String nume, String detalii, Float acuratete, int nrPredictii) {

        this.Id = id;
        this.nume = nume;
        this.Detalii = detalii;
        this.Acuratete = acuratete;
        this.NrPredictii = nrPredictii;
    }




}
