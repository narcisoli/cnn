package com.example.narcis.detecto;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import com.example.narcis.detecto.models.Masina;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StaticValues {
    public static List<Masina> lista;

    public static List<String> getLabels() {
       List<String> l= new ArrayList<>();
       for(int i=0;i<lista.size();i++)
           l.add(lista.get(i).getNume());
       return l;
    }

    public static Bitmap rotate(Bitmap bitmap,int grade)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(grade);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }
}
