package com.example.narcis.detecto.fragmente;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.narcis.detecto.MainActivity;
import com.example.narcis.detecto.R;
import com.example.narcis.detecto.StaticValues;
import com.example.narcis.detecto.models.Clasificare;
import com.example.narcis.detecto.models.Masina;

import java.io.IOException;

public class fragUpload extends Fragment {

    private static fragUpload instance;
    private TextView buttonUpload;
    private TextView takePhotoButton;
    private TextView textView;
    private ImageView imageView;
    private CardView cardPred;
    private CardView cardAccuracy;
    private ImageView thumbsUp;
    private ImageView thumbsDown;

    String prediction;
    private Masina masina;

    public static fragUpload getInstance() {
        if (instance == null)
            instance = new fragUpload();
        return instance;
    }

    private View myView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.fragment_upload, container, false);
        textView = (TextView) myView.findViewById(R.id.textview);
        imageView = (ImageView) myView.findViewById(R.id.image);
        cardPred = (CardView) myView.findViewById(R.id.cardPrediction);
        cardAccuracy = (CardView) myView.findViewById(R.id.cardAccuracy);

        thumbsDown = (ImageView) myView.findViewById(R.id.thumbsDown);
        thumbsUp = (ImageView) myView.findViewById(R.id.thumbsUp);

        thumbsDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             setAccuracy(0);
            }
        });

        thumbsUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            setAccuracy(1);
            }
        });

        buttonUpload = (TextView) myView.findViewById(R.id.uploadButton);
        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
            }
        });
        takePhotoButton = (TextView) myView.findViewById(R.id.takePhotoButton);
        takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, 2);
                }
            }
        });

        return myView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == getActivity().RESULT_OK) {
            Bitmap bitmap;
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);


                    predict(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }


        } else if (requestCode == 2 && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            predict(bitmap);
        }

    }

    private void predict(Bitmap bitmap) {
       // int small=bitmap.getWidth();
     //   if(small>bitmap.getHeight())
     //       small=bitmap.getHeight();

      //  Bitmap dst = Bitmap.createBitmap(bitmap, 0, 0, small, small);

        Bitmap redim=Bitmap.createScaledBitmap(bitmap, 128, 128, false);




        //redim=StaticValues.rotate(redim,0);
        Clasificare clas = MainActivity.retea.recognize(redim);
        prediction = clas.getNume();
        textView.setText("My prediction is : " + clas.getNume() + "\nAccuracy : " + String.format("%.2f", clas.getProbabilitate() * 100) + "%");
        imageView.setImageBitmap(bitmap);
        cardPred.setVisibility(View.VISIBLE);
        cardAccuracy.setVisibility(View.VISIBLE);
    }

    public void setAccuracy(final int accuracy) {

        for (int i = 0; i < StaticValues.lista.size(); i++) {
                masina = StaticValues.lista.get(i);
            if (masina.getNume() == prediction) {
                masina.setAcuratete((masina.getNrPredictii() * masina.getAcuratete() + accuracy) / (masina.getNrPredictii() + 1));
                masina.setNrPredictii(masina.getNrPredictii() + 1);
                fragInfo.getInstance().adaptor.notifyDataSetChanged();
                cardAccuracy.setVisibility(View.GONE);
                cardPred.setVisibility(View.GONE);
                fragInfo.getInstance().resetAccuracy();
            }
        }
    }



}
